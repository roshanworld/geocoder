name := """geocoder"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "com.googlecode.json-simple" % "json-simple" % "1.1",
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc4",
  "com.google.code.geocoder-java" % "geocoder-java" % "0.16", 
  javaWs
)

