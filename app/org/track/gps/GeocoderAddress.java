package org.track.gps;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import play.db.DB;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.LatLng;

public class GeocoderAddress
{
    private Double lat;
    private Double lon;
    private String address = null;
    public static final String _SQL_QUERY_LOC = 
            "SELECT address, ST_Distance(geom::geography, ST_SetSRID(ST_MakePoint({0}, {1}), " +
            "4326)::geography) distance FROM env_data.geocodings WHERE " + 
            "ST_DWithin(geom::geography, ST_SetSRID(ST_MakePoint({0}, {1}), 4326)::geography, 10)";
    public static final String _SQL_INSERT_LOC =
            "INSERT INTO env_data.geocodings(address, geom) VALUES(''{0}'', ST_SetSRID(ST_MakePoint({1}, {2}), " +
            "4326))";
    final static Geocoder geocoder = new Geocoder();
    public GeocoderAddress(Double lat2, Double lon2)
    {
        this.lat = lat2;
        this.lon = lon2;
        findAddress();
    }
    public Double getLat()
    {
        return lat;
    }
    public void setLat(Double lat)
    {
        this.lat = lat;
    }
    public Double getLon()
    {
        return lon;
    }
    public void setLon(Double lon)
    {
        this.lon = lon;
    }
    
    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }
    private void findAddress()
    {
        if(!setAddressFromDB())
        {
            if(setAddressFromGeocoder())
            {
                if(!setAddressToDB())
                {
                    System.out.print("Unable to insert address " + address);
                }
            }else
            {
                System.out.print("Unable to find address for " + lat + ", " + lon);
            }
        }
    }
    
    private boolean setAddressFromDB()
    {
        Statement stmt = null;
        String query = null;
        ResultSet rs = null;
        Double distance = Double.MAX_VALUE;
        Connection connection = DB.getConnection(false);
        boolean retValue = false;
        try{
            try
            {
                stmt = connection.createStatement();
                query = MessageFormat.format(_SQL_QUERY_LOC, lon.toString(), lat.toString());
                System.out.println(query + ":" + lat + "," + lon);
                rs = stmt.executeQuery(query);
                if(rs.isBeforeFirst()){
                    while(rs.next()){
                        Double dist = rs.getDouble("distance");
                        if (dist < distance)
                        {
                            distance = dist;
                            setAddress(rs.getString("address"));
                        }
                        System.out.println("Distance: " + dist + "Address: " + rs.getString("address"));
                    }
                    retValue = true;
                }
            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }finally{
            try
            {
                System.out.println("Closing connection");
                connection.close();
            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        return retValue;
    }
    
    private boolean setAddressFromGeocoder()
    {
        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setLocation(
                new LatLng(lat.toString(), lon.toString()))
                .setLanguage("en")
                .getGeocoderRequest();
        GeocodeResponse geocoderResponse;
        try
        {
            geocoderResponse = geocoder.geocode(geocoderRequest);
            List<GeocoderResult> results = geocoderResponse.getResults();
            GeocoderResult result = results.get(0);
            setAddress(result.getFormattedAddress());
            return true;
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        
        
        return false;
    }
    
    private boolean setAddressToDB()
    {
        Statement stmt = null;
        String query = null;
        int rs;
        Double distance = Double.MAX_VALUE;
        Connection connection = DB.getConnection(false);
        boolean retValue = false;
        try{
                stmt = connection.createStatement();
                query = MessageFormat.format(_SQL_INSERT_LOC, getAddress(), lon.toString(), lat.toString());
                System.out.println(query + ":" + lat + "," + lon);
                rs = stmt.executeUpdate(query);
                System.out.println(rs + "record inserted");
                connection.commit();
                retValue = true;
        } catch (SQLException e)
        {
            try
            {
                connection.rollback();
            } catch (SQLException e1)
            {
                e1.printStackTrace();
            }
            
            e.printStackTrace();
            System.out.println(query);
        }finally{
            try
            {
                System.out.println("Closing connection");
                connection.close();
            } catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        return retValue;
        
    }
    
    public String toJSON()
    {
        JSONObject obj = new JSONObject();
        obj.put("lat", lat);
        obj.put("lon", lon);
        obj.put("address", address);
        return obj.toJSONString();
    }
}
