package controllers;

import org.track.gps.GeocoderAddress;

import play.mvc.Controller;
import play.mvc.Result;

public class GeocoderApplication extends Controller
{
    public static Result getAddress(Double lat, Double lon){
        System.out.println(lat + " " + lon);
        return ok(new GeocoderAddress(lat, lon).toJSON()).as("text/json");
    }
}
